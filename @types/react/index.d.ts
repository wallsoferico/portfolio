declare namespace JSX {
	interface IntrinsicElements {
		"e-card": React.DetailedHTMLProps<
			React.HTMLAttributes<HTMLElement>,
			HTMLElement
		> & {
			wide?: string;
		};
	}
}
