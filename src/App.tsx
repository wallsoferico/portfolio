import React from "react";
import { About } from "./about/About";
import "./App.css";
import Home from "./Home";
import { Skills } from "./skills/Skills";
import { Experience } from "./experience/Experience";
import { Contact } from "./contact/Contact";
import { Footer } from "./footer/Footer";

const App: React.FC = () => {
	return (
		<div>
			<Home />
			<Skills />
			<About />
			<Experience />
			<Contact />
			<Footer />
		</div>
	);
};

export default App;
