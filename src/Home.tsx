import "./App.css";

import React from "react";

import { Avatar } from "./header/Avatar";
import { SocialBar } from "./common/SocialBar";
import { NavBar } from "./nav/NavBar";
import { IntroText } from "./header/IntroText";

const Home: React.FC = () => {
	return (
		<div id="home">
			<NavBar />
			<header className="home_wrapper">
				<IntroText />
				<Avatar />
				<span className="bottom_border"></span>
				<SocialBar />
			</header>
		</div>
	);
};

export default Home;
