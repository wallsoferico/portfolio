import "./About.css";

import React, { Component } from "react";
import { SocialBar } from "../common/SocialBar";
import { ResumeButton } from "../common/ResumeButton";

export class About extends Component {
	render() {
		return (
			<section className="about_wrapper" id="about">
				<h1>Who I Am</h1>
				<p className="about_details">
					I have worked as a Full Stack Developer for over 6 years
					now. I enjoy a challenge and learning something new, honing
					my skills, improving myself. I strive to be versatile,
					efficient, and dedicated.
				</p>
				<div className="about_social">
					<ResumeButton />
					<SocialBar />
				</div>
			</section>
		);
	}
}
