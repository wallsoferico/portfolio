import React, { Component } from "react";
import "./Button.css";

export interface IButtonProps
	extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	name: string;
	onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
	color?: string;
}

export class Button extends Component<IButtonProps> {
	render() {
		return (
			<button
				className={
					this.props.color ? "btn btn-" + this.props.color : "btn"
				}
				onClick={this.props.onClick}
				title={this.props.name}
				{...this.props}>
				{this.props.children}
			</button>
		);
	}
}
