import React, { Component } from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface CardProps {
	title: string;
	icon?: IconProp;
	wide?: string;
	color?: string;
}

export class Card extends Component<CardProps> {
	render() {
		return (
			<e-card color={this.props.color} {...this.props}>
				{this.props.icon && (
					<span slot="icon">
						<FontAwesomeIcon icon={this.props.icon} size="2x" />
					</span>
				)}
				<span slot="title">{this.props.title}</span>
				<span slot="details">{this.props.children}</span>
			</e-card>
		);
	}
}

export class CardComponent extends HTMLElement {
	[index: string]: any;

	private template = `
        <style>
            :host {
                display: inline-block;
                font-family: "Open Sans", sans-serif;
                font-size: 16px;
                line-height: 26px;
                text-rendering: optimizeLegibility;
                box-sizing: border-box;
                width: 15em;
                height: auto;
                border-top-left-radius: 1%;
                border-top-right-radius: 1%;
                border-bottom-left-radius: 5%;
                border-bottom-right-radius: 25%;
                background-color: #fff;
                transition: all 0.5s;
                position: relative;
                box-shadow: 1px 1px 10px #ccc;
                padding: 1em;
                text-align: left;
            }

            aside {
                width: 100%;
                height: 100%;
                display: grid;
                grid-template-areas: 
                    "icon title"
                    "blank details";
                grid-template-columns: 0.5fr 2fr;
                grid-template-rows: 0.5fr 1fr;
                align-items: center;
                overflow: hidden;
            }

            :host(:hover) {
                box-shadow: 2px 2px 10px #999;
            }

            :host([wide]) {
                width: 100%;
                border-radius: 1%;
            }

            .card_icon {
                grid-area: icon;
                z-index: 2;
            }

            .card_header {
                font-size: 1.5em;
                line-height: 2em;
                z-index: 2;
                grid-area: title;
            }

            .card_details {
                align-self: start;
                font-size: 0.8em;
                line-height: 1.2em;
                z-index: 2;
                grid-area: details;
            }

            e-card:not(:defined) {
                /* Pre-style, give layout, replicate app-drawer's eventual styles, etc. */
                display: inline-block;
                opacity: 0;
                transition: opacity 0.3s ease-in-out;
            }
        </style>

        <aside>
            <div class="card_icon">
                <slot name="icon"></slot>
            </div>
            <div class="card_header">
                <slot name="title"></slot>
            </div>
            <div class="card_details">
                <slot name="details"></slot>
            </div>
        </aside>        
    `;

	get color(): string | null {
		return this.getAttribute("color");
	}

	set color(val: string | null) {
		if (val) {
			this.setAttribute("style", "--card-primary-color: " + val);
			this.setAttribute("color", val);
		} else {
			this.removeAttribute("style");
			this.removeAttribute("color");
		}
	}

	get wide(): boolean {
		return this.hasAttribute("wide");
	}

	set wide(val: boolean) {
		if (val) {
			this.setAttribute("wide", "");
		} else {
			this.removeAttribute("wide");
		}
	}

	constructor() {
		super();

		this.generateTemplate();

		this.initAttributes();
	}

	private generateTemplate() {
		const tmpl = document.createElement("template");

		tmpl.innerHTML = this.template;

		const shadowRoot = this.attachShadow({ mode: "open" });
		shadowRoot.appendChild(tmpl.content.cloneNode(true));
	}

	private initAttributes() {
		Array.from(this.attributes).forEach((attr) =>
			this.upgradeProperty(attr.name)
		);
	}

	private upgradeProperty(prop: string) {
		if (this.hasOwnProperty(prop)) {
			let value = this[prop];
			delete this[prop];
			this[prop] = value;
		}
	}
}

customElements.define("e-card", CardComponent);
