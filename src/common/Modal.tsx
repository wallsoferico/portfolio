import React, { Component } from "react";
import { Button } from "./Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faTimes } from "@fortawesome/pro-light-svg-icons";

import "./Modal.css";

library.add(faTimes);

interface IModalProps extends React.HTMLAttributes<any> {
	show?: boolean;
	onClose: (e: React.MouseEvent) => void;
}

export class Modal extends Component<IModalProps> {
	private modalRef: React.RefObject<HTMLDivElement>;

	constructor(props: React.ComponentProps<any>) {
		super(props);

		this.modalRef = React.createRef();
		this.onCloserHandler = this.onCloserHandler.bind(this);
	}

	onCloserHandler(e: React.MouseEvent) {
		this.modalRef.current?.classList.add("off");

		setTimeout(() => this.props.onClose(e), 500);
	}

	render() {
		if (!this.props.show) {
			return null;
		}

		return (
			<div className="modal_wrapper" ref={this.modalRef}>
				<main className="modal_container">
					<header>
						<h1 className="nav_title">E</h1>

						<span
							className="modal_close"
							onClick={this.onCloserHandler}>
							<FontAwesomeIcon
								icon={["fal", "times"]}
								fixedWidth
								size="2x"
								color="#111"
							/>
						</span>
					</header>
					<section className="modal_content">
						{this.props.children}
					</section>
					<footer>
						<Button
							onClick={this.onCloserHandler}
							name="Close"
							color="gray">
							Close
						</Button>
					</footer>
				</main>
			</div>
		);
	}
}
