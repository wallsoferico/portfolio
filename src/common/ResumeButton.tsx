/// <reference path="../../@types/resume/index.d.ts" />
import React, { Component } from "react";
import { Button } from "./Button";
import { Modal } from "./Modal";

import Resume from "../assets/EricShaw_Resume.pdf";

import "./ResumeButton.css";

interface IResumeState {
	show?: boolean;
}

export class ResumeButton extends Component<{}, IResumeState> {
	constructor(props: React.ComponentProps<any>) {
		super(props);

		this.toggleModalHandler = this.toggleModalHandler.bind(this);

		this.state = {
			show: false
		};
	}

	toggleModalHandler(e: React.MouseEvent) {
		console.log(e);

		this.setState({
			show: !this.state.show
		});
	}

	render() {
		return (
			<>
				<Button
					onClick={this.toggleModalHandler}
					name={"See my resume"}>
					Resume
				</Button>
				<Modal show={this.state.show} onClose={this.toggleModalHandler}>
					<object
						className="resume_container"
						data={Resume}
						type="application/pdf"
						width="100%"
						height="100%">
						<a href="../assets/EricShaw_Resume.pdf" download>
							Download My Resume
						</a>
					</object>
				</Modal>
			</>
		);
	}
}
