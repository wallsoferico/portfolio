import { library } from "@fortawesome/fontawesome-svg-core";
import {
	fab,
	faGithub,
	faGitlab,
	faLinkedinIn
} from "@fortawesome/free-brands-svg-icons";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import React, { Component } from "react";
import { SocialButton } from "./SocialButton";
import { StackedSocialButton } from "./StackedSocialButton";

import "./SocialBar.css";

library.add(fab, faGithub, faGitlab, faLinkedinIn, faCircle);

export class SocialBar extends Component {
	render() {
		return (
			<aside>
				<ul className="social_bar">
					<li>
						<SocialButton
							icon={["fab", "github"]}
							href={"https://github.com/wallsoferico"}
							alt={"@wallsoferico GitHub"}
						/>
					</li>
					<li>
						<StackedSocialButton
							icon={["fab", "gitlab"]}
							href={"https://gitlab.com/wallsoferico"}
							alt={"@wallsoferico GitLab"}
						/>
					</li>
					<li>
						<StackedSocialButton
							icon={["fab", "linkedin-in"]}
							href={"https://www.linkedin.com/in/eric-shaw-dev/"}
							alt={"LinkedIn"}
						/>
					</li>
				</ul>
			</aside>
		);
	}
}
