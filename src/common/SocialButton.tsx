import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Component } from "react";
import React from "react";

export interface ISocialButtonProps {
	href: string;
	icon: IconProp;
	alt: string;
}

export class SocialButton extends Component<ISocialButtonProps> {
	render() {
		return (
			<a
				href={this.props.href}
				target="_blank"
				rel="noopener noreferrer"
				title={this.props.alt}
				className="social_icon_wrapper fa-layers fa-fw fa-2x">
				<FontAwesomeIcon
					icon={this.props.icon}
					fixedWidth
					title={this.props.alt}
					color="#24292e"
					className="social_icon-dark"
				/>
			</a>
		);
	}
}
