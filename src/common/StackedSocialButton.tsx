import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { ISocialButtonProps } from "./SocialButton";

export class StackedSocialButton extends Component<ISocialButtonProps> {
	render() {
		return (
			<a
				href={this.props.href}
				target="_blank"
				rel="noopener noreferrer"
				title={this.props.alt}
				className="social_icon_wrapper fa-layers fa-fw fa-2x">
				<FontAwesomeIcon icon="circle" className="social_icon-dark" />
				<FontAwesomeIcon
					icon={this.props.icon}
					fixedWidth
					title={this.props.alt}
					inverse
					transform="shrink-5"
				/>
			</a>
		);
	}
}
