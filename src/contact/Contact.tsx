import "./Contact.css";

import React, { Component } from "react";
import { Button } from "../common/Button";

interface IContactState {
	valid: boolean;
}

export class Contact extends Component<{}, IContactState> {
	private formRef: React.RefObject<HTMLFormElement>;

	constructor(props: React.ComponentProps<any>) {
		super(props);

		this.formRef = React.createRef();

		this.onFormChange = this.onFormChange.bind(this);

		this.state = {
			valid: false
		};
	}

	onFormChange(e: React.FormEvent<HTMLFormElement>) {
		if (!this.formRef.current) return;

		this.setState({
			valid: this.formRef.current.checkValidity()
		});
	}

	render() {
		return (
			<section className="contact_wrapper" id="contact">
				<h1 className="title_underline">Contact</h1>
				<form
					action="https://formspree.io/mqkpwjrn"
					method="POST"
					id="contactForm"
					ref={this.formRef}
					onChange={this.onFormChange}>
					<label>Email:</label>
					<input
						type="email"
						name="email"
						inputMode="email"
						autoComplete="email"
						required={true}
					/>
					<label>Subject:</label>
					<input name="_subject" type="text" required={true} />
					<label>Message:</label>
					<textarea name="message" required={true} />
					<input
						type="hidden"
						name="_next"
						value="https://ericshaw.dev/"
					/>
					<Button
						name="Send Email"
						color="small"
						disabled={!this.state.valid}>
						Send
					</Button>
				</form>
			</section>
		);
	}
}
