import React, { Component } from "react";
import ClearpoolLogo from "../assets/clearpoolLogo.png";

export class ClearpoolAssociateModalContent extends Component {
	render() {
		return (
			<main className="experience_modal">
				<img
					src={ClearpoolLogo}
					alt="Clearpool Group Logo"
					className="experience_logo"
				/>
				<span>
					<h1>Clearpool Group</h1>
					<small>Associate Developer (Nov 2016 – Oct 2018)</small>
				</span>
				<ul>
					<li>
						Participated in the design and requirement planning for
						new features.
					</li>
					<li>Developed solutions for bugs and new requirements.</li>
					<li>
						Monitored application logs and diagnosed any issues that
						arise.
					</li>
					<li>Updated web pages based on new design template.</li>
				</ul>
			</main>
		);
	}
}
