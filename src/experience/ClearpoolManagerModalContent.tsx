import React, { Component } from "react";
import ClearpoolLogo from "../assets/clearpoolLogo.png";

export class ClearpoolManagerModalContent extends Component {
	render() {
		return (
			<main className="experience_modal">
				<img
					src={ClearpoolLogo}
					alt="Clearpool Group Logo"
					className="experience_logo"
				/>
				<span>
					<h1>Clearpool Group</h1>
					<small>Dev Manager (Oct 2018 - Present)</small>
				</span>
				<ul>
					<li>
						Manage the web team, consisting of 2 other developers.
						Spec out tickets, write implementation details and
						testing scenarios.
					</li>
					<li>
						Design and implement user oriented, responsive web pages
						based on specked out details.
					</li>
					<li>
						Migrate legacy AngularJS pages to TypeScript, reducing
						dependencies on large frameworks and libraries by
						leveraging newer technologies such as WebComponents.
					</li>
					<li>
						Foster a supportive environment that encourages
						ingenuity, creativity, and advancement.
					</li>
					<li>
						Create LeapWork test Flows for end to end testing, built
						out helper sub flows to simplify test creation.
					</li>
					<li>
						Built out reusable WebComponents using vanilla
						JavaScript to maintain consistent design standards and
						functionality.
					</li>
					<li>
						Migrated from gulp to Webpack to streamline the front
						end build process.
					</li>
					<li>
						Managed two interns, teaching them the software
						development process, JavaScript and CSS.
					</li>
				</ul>
			</main>
		);
	}
}
