import React, { Component } from "react";
import DealogicLogo from "../assets/dealogicLogo.png";

export class DealogicModalContent extends Component {
	render() {
		return (
			<main className="experience_modal">
				<img
					src={DealogicLogo}
					alt="Dealogic Logo"
					className="experience_logo"
				/>
				<span>
					<h1>Dealogic</h1>
					<small>Software Engineer (Jun 2013 - Aug 2013)</small>
				</span>
				<ul>
					<li>Maintain/Rewrite existing ASP legacy pages to C#.</li>
					<li>
						Used AngularJS, jQuery, Kendo UI, and Bootstrap to
						develop and design .NET pages.
					</li>
					<li>
						Created AngularJS directives to simplify and standardize
						page development as well as extend the functionality of
						Kendo UI and Bootstrap components such as, a custom
						column selector to replace the default Kendo Grid column
						selector and an external filter for Kendo Grid.
					</li>
					<li>
						Supported an application using the Model View View-Model
						design (MVVM).
					</li>
					<li>
						Created and Maintained Repository functions using
						NHibernate’s Query Over to improve performance of
						complex pages.
					</li>
					<li>
						Created integration tests using SpecFlow to test
						integration between MVC controllers and the repository.
					</li>
					<li>
						Created unit tests using QUnit to more easily test
						AngularJS services.
					</li>
					<li>
						Supported, Developed, and Maintained a QlikView
						application and related QlikVIew Scripts.
					</li>
					<li>
						Headed the intern program for my team, mentored interns
						and taught them basic principles of MVC, C#, NHibernate,
						AngularJS, Bootstrap, CSS, HTML and JavaScript
					</li>
				</ul>
			</main>
		);
	}
}
