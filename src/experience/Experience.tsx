import "./Experience.css";

import React, { Component } from "react";
import ClearpoolLogo from "../assets/clearpoolLogo.png";
import DealogicLogo from "../assets/dealogicLogo.png";
import { ClearpoolManagerModalContent } from "./ClearpoolManagerModalContent";
import { ExperienceCard } from "./ExperienceCard";
import { ClearpoolAssociateModalContent } from "./ClearpoolAssociateModalContent";
import { DealogicModalContent } from "./DealogicModalContent";

interface ExperienceModals {
	managerModal: JSX.Element;
	associateModal: JSX.Element;
	dealogicModal: JSX.Element;
}

export class Experience extends Component {
	private modals: ExperienceModals;

	constructor(props: React.ComponentProps<any>) {
		super(props);

		this.modals = {
			managerModal: <ClearpoolManagerModalContent />,
			associateModal: <ClearpoolAssociateModalContent />,
			dealogicModal: <DealogicModalContent />
		};
	}

	render() {
		return (
			<section className="experience_wrapper" id="experience">
				<h1 className="title_underline">Experience</h1>
				<ul className="list-unstyled experience_list">
					<li>
						<ExperienceCard
							title="Clearpool Group"
							iconSrc={ClearpoolLogo}
							iconAlt={"Clearpool Group Logo"}
							modalContent={this.modals.managerModal}>
							<small>Dev Manager (Oct 2018 - Present)</small>
							<p>
								Manage a team consisting of up to 2 developers,
								where I design and implement user orented and
								responsive web pages. We use TypeScript,
								WebComponents, Webpack, HTML and CSS.
							</p>
						</ExperienceCard>
					</li>
					<li>
						<ExperienceCard
							title="Clearpool Group"
							iconSrc={ClearpoolLogo}
							iconAlt={"Clearpool Group Logo"}
							modalContent={this.modals.associateModal}>
							<small>
								Associate Developer (Nov 2016 – Oct 2018)
							</small>
							<p>
								Designed and built web pages based on
								requirements gathered during planning. Developed
								solutions for bugs and new requirements
							</p>
						</ExperienceCard>
					</li>
					<li>
						<ExperienceCard
							title="Dealogic"
							iconSrc={DealogicLogo}
							iconAlt={"Dealogic Logo"}
							modalContent={this.modals.dealogicModal}>
							<small>
								Software Engineer (Jun 2013 - Aug 2013)
							</small>
							<p>
								Application development in C#, performance
								monitoring and improvements in application
								pages, web page to better manage language
								translations
							</p>
						</ExperienceCard>
					</li>
				</ul>
			</section>
		);
	}
}
