import React, { Component } from "react";
import { Modal } from "../common/Modal";
import { Button } from "../common/Button";

interface ExperienceCardProps {
	title: string;
	iconSrc: string;
	iconAlt: string;
	modalContent: JSX.Element;
}

interface IModalState {
	show: boolean;
}

export class ExperienceCard extends Component<
	ExperienceCardProps,
	IModalState
> {
	constructor(props: React.ComponentProps<any>) {
		super(props);

		this.toggleModalHandler = this.toggleModalHandler.bind(this);

		this.state = {
			show: false
		};
	}

	toggleModalHandler(e: React.MouseEvent) {
		this.setState({
			show: !this.state.show
		});
	}

	render() {
		return (
			<>
				<e-card wide="">
					<span slot="icon">
						<img
							src={this.props.iconSrc}
							alt={this.props.iconAlt}
							className="experience_logo"
						/>
					</span>
					<span slot="title">{this.props.title}</span>
					<span slot="details">
						{this.props.children}
						<Button
							name="Show More Info"
							color="transparent"
							onClick={this.toggleModalHandler}>
							More Info
						</Button>
					</span>
				</e-card>

				<Modal show={this.state.show} onClose={this.toggleModalHandler}>
					{this.props.modalContent}
				</Modal>
			</>
		);
	}
}
