import "./Footer.css";

import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCopyright } from "@fortawesome/pro-light-svg-icons";

library.add(faCopyright);

export class Footer extends Component {
	render() {
		return (
			<footer className="page_footer">
				<FontAwesomeIcon
					icon={["fal", "copyright"]}
					color="#fff"
					fixedWidth
				/>
				<span className="copyright_text">
					Eric Shaw {new Date().getFullYear()}. All rights reserved.
				</span>
			</footer>
		);
	}
}
