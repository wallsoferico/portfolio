import "./Avatar.css";

import React, { Component } from "react";

import avatar from "../assets/avatar.jpg";
import { ReactComponent as AvatarSVG } from "../assets/clipPath.svg";

export class Avatar extends Component {
	render() {
		return (
			<figure>
				<div className="avatar_border"></div>
				<img
					className="avatar"
					src={avatar}
					alt="Eric Shaw with his dog Lychee"
				/>
				<AvatarSVG />
			</figure>
		);
	}
}
