import "./IntroText.css";

import React, { Component } from "react";

export class IntroText extends Component {
	render() {
		return (
			<ul className="intro_text_wrapper">
				<li className="intro_text-title">Hello,</li>
				<li className="intro_text-sub">I am Eric Shaw</li>
				<li className="intro_text-details">
					An experienced Full Stack Software Developer interested in
					translating a passion for web technologies such as
					TypeScript, ES5/6, Mithril and Web Components into a new
					role creating user oriented and responsive web applications.
				</li>
			</ul>
		);
	}
}
