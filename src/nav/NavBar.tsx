import React, { Component } from "react";
import { NavTitle } from "./NavTitle";
import { NavLinks } from "./NavLinks";

import "./NavBar.css";

export class NavBar extends Component {
	render() {
		return (
			<nav>
				<NavTitle />
				<NavLinks />
			</nav>
		);
	}
}
