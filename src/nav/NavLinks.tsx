import React, { Component } from "react";
import "./NavLinks.css";

export class NavLinks extends Component {
	render() {
		return (
			<ul className="nav_link_wrapper">
				<li>
					<a className="nav_link" href="#home">
						Home
					</a>
				</li>
				<li>
					<a className="nav_link" href="#about">
						About
					</a>
				</li>
				<li>
					<a className="nav_link" href="#experience">
						Experience
					</a>
				</li>
				<li>
					<a className="nav_link" href="#contact">
						Contact
					</a>
				</li>
			</ul>
		);
	}
}
