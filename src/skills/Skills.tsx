import React, { Component } from "react";
import { Card } from "../common/Card";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faBrowser,
	faServer,
	faCode
} from "@fortawesome/pro-duotone-svg-icons";

import { faPlus, faEquals } from "@fortawesome/pro-light-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Skills.css";

library.add(faBrowser, faServer, faCode, faPlus, faEquals);

export class Skills extends Component {
	render() {
		return (
			<ul className="skills_wrapper list-unstyled list-inline">
				<li>
					<Card title={"Front End"} icon={["fad", "browser"]}>
						JavaScrip, TypeScript, D3, CSS, HTML 5, ES5/6,
						WebComponents, Webpack
					</Card>
				</li>
				<li>
					<FontAwesomeIcon icon={["fal", "plus"]} size="2x" />
				</li>
				<li>
					<Card title={"Back End"} icon={["fad", "server"]}>
						Java, SQL, Java Spring
					</Card>
				</li>
				<li>
					<FontAwesomeIcon icon={["fal", "equals"]} size="2x" />
				</li>
				<li>
					<Card title={"Full Stack"} icon={["fad", "code"]}>
						Versatile, eager, effective
					</Card>
				</li>
			</ul>
		);
	}
}
